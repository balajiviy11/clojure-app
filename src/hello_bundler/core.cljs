;; (ns hello-bundler.core
;;   (:require [react]))

;; (defn ^:dev/after-load start []
;;   (.appendChild (js/document.getElementById "app")
;;                 (js/document.createTextNode "Hello, world")))


;; (js/alert "heeeee")
(ns hello-bundler.core)

(def trap-types
  "List of the different kinds of traps we can
  have (useful for iterating over them)"
  '(square cross plus diamond octagon))

(defn position-type [coords]
  "This function takes in coordinates of the form '(x y)
  and returns a 2-tuple describing what kind of position
  it is.
  
  The first item can be one of the following:
  
    - nil means it's an invalid position. The second term
      will describe why exactly it's invalid
    - 'cell means it's a place where pieces can move. The
      second term tells you whether it's for 'traps
      or 'stones
    - 'floating is a special position for pieces that are
      not yet placed on the board, but still 'floating'
      with the player. The second term should (usually)
      indicate which player's floating pieces these are."
  (cond 
    ;; take care of the floating values first
    (= (first coords) 'floating) (list 'floating (second coords))
        
    ;; from this point on, there should be two coordinates...
    (nil? coords) (list nil 'invalid)

    ;; ...and both "coordinates" should be numbers
    (not-every? #'number? coords) (list nil 'invalid)

    ;; make sure we're within the board boundaries!
    (some (fn [x] (or (< x 0) (> x 18))) coords) (list nil 'out-of-bounds)

    ;; only alternate "squares" are used in this game; the other half are unused
    (not (= (mod (first coords) 2) (mod (second coords) 2))) (list nil 'unused)

    ;; of the remaining coordinates, every alternate one is a stone
    (= (mod (first coords) 2) 1) (list 'cell 'stones)

    ;; special case: remove the corner trap-cells!
    (every? (fn [x] (or (= x 0) (= x 18))) coords) (list nil 'unused)

    ;; everything else is taken care of, so this must be a trap
    :else (list 'cell 'traps)))


;; Just some testing
(= (position-type '(2 3)) '(nil unused))
(= (position-type '(0 2)) '(cell traps))
(= (position-type '(-2 -2)) '(nil out-of-bounds))
(= (position-type '(2 20)) '(nil out-of-bounds))
(= (position-type '(14 6)) '(cell traps))
(= (position-type '(5 7)) '(cell stones))
(= (position-type '(20 0)) '(nil out-of-bounds))

(println "Testing: position-type")
(println (position-type nil))

;; Roop direction -> coordinates
(def roop-dir-to-coords
  "Maps the 'natural' directions like east, west, north,
  south to coordinates. The idea is that we can write
  a few low-level functions to handle the coordinates,
  and then do all our other coding using natural
  directions without worrying about coordinates at all.
  
  This is especially useful in a complicated board like
  Roop's where calculating coordinates can be a pain!
  
  You'll know which functions use coordinates because
  they'll use these directions during calculation.
  Other functions will only take the directions to
  pass them on to functions that do the actual
  calculation."
  {'north '(0 -2)
   'south '(0 2)
   'east '(2 0)
   'west '(-2 0)
   'north-east '(2 -2)
   'north-west '(-2 -2)
   'south-east '(2 2)
   'south-west '(-2 2)})

(def roop-dir-for-corners
  "This is for calculating the corners for the piece."
  {
   'north-east '(1 -1)
   'north-west '(-1 -1)
   'south-east '(1 1)
   'south-west '(-1 1)}
)

(defn direction->antidirection [coords]
  "Returns the inverse of every direction (north will
  become south, east will become west, and so on, but
  with coordinates because human-readble is complicated.
  So please pass (transformation/direction) coordinates
  only; I'm not sure what this function will do with a
  human-readable value! (Hopefully an error)."
  (list
    (- 0 (first coords))
    (- 0 (second coords))))

(= (direction->antidirection (roop-dir-to-coords 'north)) '(0 2))

(defn valid-position-or-nil [coords]
  "Some functions just calculate a position without
  checking if it's valid. You can pass them through
  this function to guarantee that the position is
  actually valid.
  
  If the coordinates are not valid, it will return nil
  instead so you'll immediately know there's something
  wrong."
  (if (= (first (position-type coords)) nil) nil
    coords))

(println "Testing: valid-position-or-nil")
(println (valid-position-or-nil nil))

(defn direction->coordinates
  "Takes in a starting location and a direction,
  and calculates the coordinates for a step in that
  direction.
  
  This function handles wrapping but it does NOT check
  if the final position is valid! Please use the sister
  function direction->valid-coordinates for that. We have kept
  the functionality separate for easier debugging."
  ([start direction dir-to-coords]
    (direction->coordinates start direction false dir-to-coords))
  ([start direction wrap dir-to-coords]
    (let [coords (dir-to-coords direction)
          row (+ (first start) (first coords))
          col (+ (second start) (second coords))]
          (if wrap
            (condp = (second (position-type start))
              'traps (list (mod row 20) (mod col 20))
              'stones (list (mod row 18) (mod col 18))
              (list row col))
            (list row col)))))

;; Just some testing
(= (direction->coordinates '(8 6) 'north-east roop-dir-to-coords) '(10 4))
(= (direction->coordinates '(8 6) 'north-east true roop-dir-to-coords) '(10 4))
(= (direction->coordinates '(10 10) 'south roop-dir-to-coords) '(10 12))
(= (direction->coordinates '(15 15) 'south true roop-dir-to-coords) '(15 17))
(= (direction->coordinates '(0 2) 'north true roop-dir-to-coords) '(0 0))
(= (direction->coordinates '(0 2) 'north roop-dir-to-coords) '(0 0))
(= (direction->coordinates '(25 26) 'north roop-dir-to-coords) '(25 24))
(= (direction->coordinates '(25 26) 'north true roop-dir-to-coords) '(25 24))
(= (direction->coordinates '(15 17) 'south true roop-dir-to-coords) '(15 1))

(defn direction->valid-coordinates
  "Same as direction->valid-coordinates, but only if the start and final
  direction are valid"
  ([start direction dir-to-coords]
    (direction->valid-coordinates start direction false dir-to-coords))
  ([start direction wrap dir-to-coords]
    (if (and (valid-position-or-nil start) (dir-to-coords direction))
        (valid-position-or-nil (direction->coordinates start direction wrap dir-to-coords))
        nil)))

(println "Testing: direction->valid-coordinates")
(println (direction->valid-coordinates '(0 12) nil roop-dir-to-coords))


;; More testing
(= (direction->valid-coordinates '(8 6) 'north-east roop-dir-to-coords) '(10 4))
(= (direction->valid-coordinates '(8 6) 'north-east true roop-dir-to-coords) '(10 4))
(= (direction->valid-coordinates '(10 10) 'south roop-dir-to-coords) '(10 12))
(= (direction->valid-coordinates '(15 15) 'south true roop-dir-to-coords) '(15 17))
(= (direction->valid-coordinates '(0 2) 'north true roop-dir-to-coords) nil)
(= (direction->valid-coordinates '(0 2) 'north roop-dir-to-coords) nil)
(= (direction->valid-coordinates '(25 26) 'north roop-dir-to-coords) nil)
(= (direction->valid-coordinates '(25 26) 'north true roop-dir-to-coords) nil)
(= (direction->coordinates '(15 17) 'south true roop-dir-to-coords) '(15 17))


;; Syntax for pieces:
;; '(type colour subtype)
;; TODO: actually define these types somehow

;; Possible subtypes for traps:
;; - square
;; - octagon
;; - diamond
;; - plus
;; - cross
;;
;; Pieces don't have subtypes

;; Sample board for testing
(def pieces-on-position
  "Stores information on which pieces are there,
  for every square on the board. Only the occupied
  squares are there."
  { '( 6  4) ['(trap blue diamond)]
    '(14  4) ['(trap green diamond)]
    '(12  8) ['(trap blue cross)]
    '( 8 14) ['(trap blue square)]
    '(10 14) ['(trap green plus)]
    '( 6 16) ['(trap green octagon)]
    '( 4 18) ['(trap green square)]
    '( 9  5) ['(stone blue)]
    '(11  5) ['(stone green) '(bag blue) '(dot)]
    '( 1  7) ['(stone blue)]
    '( 5  7) ['(stone duck)]
    '(15  9) ['(stone green)]
    '( 7 11) ['(stone green) '(bag blue)]
    '(13 11) ['(stone blue) '(dot) '(bag green)]
    '(13 15) ['(stone blue)]
    '(15 15) ['(stone green)]
    '( 7  7) ['(dot)]
    '( 9  7) ['(dot)]
    '( 3  9) ['(dot)]
    '( 5  9) ['(dot)]
    '( 9 11) ['(dot)]
    ;; added new pieces for testing capture
    '(13 17) ['(stone blue)]
    '(12 16) ['(trap green octagon)]
    '( 7 15) ['(stone green)]
    '(11 15) ['(stone blue)]
    '( 8 12) ['(trap blue square)]
    '( 9 13) ['(stone blue)]
  })

(defn position-contains? [position piece-types pieces-on-position]
  "Tells if the given position contains any pieces of the
  given type in the given board"
  (some #(contains? piece-types (first %)) (pieces-on-position position)))

;; did it work?
(= (position-contains? '(9 5) #{ 'stone } pieces-on-position) true)
(= (position-contains? '(12 8) #{ 'stone } pieces-on-position) nil)
(= (position-contains? '(1 7) #{ 'stone } pieces-on-position) true)
(= (position-contains? '(11 5) #{ 'stone 'bag } pieces-on-position) true)
(= (position-contains? '(7 5) #{ 'trap } pieces-on-position) nil)
(= (position-contains? '(7 7) #{ 'stone } pieces-on-position) nil )
(= (position-contains? '(7 9) #{ 'dot } pieces-on-position) nil)


(= (position-contains? '(15 15) #{ 'stone 'trap } pieces-on-position) true)
(= (position-contains? '(3 18) #{ 'trap 'dot } pieces-on-position) nil)
(= (position-contains? '(4 12) #{ 'trap } pieces-on-position) nil)
(= (position-contains? '(10 14) #{ 'trap } pieces-on-position) true)
(= (position-contains? '(7 11) #{ 'bag } pieces-on-position) true)

(defn difference
  "Calculate the difference between two numbers. It will subtract one
  from the other, and make the value positive if it turns out
  negative."
  [x y]
  (let [diff (- x y)]
    (if (neg? diff)
        (- 0 diff)
        diff)))

;; Testing
(= (difference 4 3) 1)
(= (difference -2 10) 12)
(= (difference 3 4) 1)


(defn corner-between [start end]
  "Gives the corner between to adjacent squares, if they
  share a common corner. If there is no common corner
  between the two squares, the function returns nil.
  
  By this function does not take care of wraps. But that
  doesn't matter because there are never 'in-between'
  corners while wrapping. Think about it ;-)"
  (let [start-x (first start)
        start-y (second start)
        end-x (first end)
        end-y (second end)
        x-distance (difference start-x end-x)
        y-distance (difference start-y end-y)]

        ;; If there's a common corner, both x-distance and
        ;; y-distance will be 2
        (if (and (= y-distance 2) (= x-distance 2))

             ;; Calculate the half-way mark...
             (let [corner-x (+ start-x (/ (- end-x start-x) 2))
                  corner-y (+ start-y (/ (- end-y start-y) 2))]

                  ;; ...and return it
                  (list corner-x corner-y))

              ;; If it's not a corner, we just return nil
              nil)))

;; Some testing
(println "roop" (corner-between '(10 14) '(12 16)) '(11 15))
(= (corner-between '(11 15) '(13 13)) '(12 14))
(= (corner-between '(11 15) '(15 11)) nil)
(= (corner-between '(12 16) '(14 14)) '(13 15))
(= (corner-between '(18 12) '(10 18)) nil)
(= (corner-between '(8 16) '(6 18)) '(7 17))
(= (corner-between '(8 14) '(6 16)) '(7 15))

(defn clear-pathway? [start end pieces-on-position]
  "Only for adjacent squares: this function looks at the board
  and checks if there's a clear pathway between the two given
  squares. Return value is true or false (as is the case for any
  function ending with a question mark!)
  
  Warning: if the squares are not adjacent, this function will
  malfunction!"
  
  ;; First, check if there is a corner. If not, it's always clear.
  (let [corner (corner-between start end)]
       (if corner
           (not (position-contains? corner #{ 'stone 'trap 'bag } pieces-on-position))
           
           ;; If there's no corner, it's always clear <-- take note!
           true)))


(= (clear-pathway? '(11 7) '(13 9) pieces-on-position) false)
(= (clear-pathway? '(12 8) '(10 6) pieces-on-position) true)
(= (clear-pathway? '(6 16) '(4 18) pieces-on-position) true)
(= (clear-pathway? '(6 16) '(8 14) pieces-on-position) true)
(= (clear-pathway? '(15 9) '(13 11) pieces-on-position) true)
(= (clear-pathway? '(12 8) '(12 6) pieces-on-position) true)
(= (clear-pathway? '(16 10) '(14 8) pieces-on-position) false)
(= (clear-pathway? '(12 12) '(14 10) pieces-on-position) false)
(= (clear-pathway? '(5 5) '(7 3) pieces-on-position) false)
(= (clear-pathway? '(15 5) '(13 3) pieces-on-position) false)

(def movement-for-pieces
  "Stores the movement information for each piece. Every key
  is a piece type (or subtype in the case of traps) and the
  value is a tuple of the possible moves and the number of
  steps it's allowed to take.

  Possible moves need a bit more explanation. The sub-hash
  for them is as follows:

  Every key (for the possible moves) is a possible first move
  for the piece, and the value is the list of moves possible
  if you made that previous move. For example,

    'north [ 'north-east 'north-west ]

  means that if you moved north in the previous move
  then you can move either north-east or north-west
  in the previous move."

  {
    'stone '({
      north [north]
      south [south]
      east [east]
      west [west]
      north-east [north-east]
      north-west [north-west]
      south-east [south-east]
      south-west [south-west]}
      1)

    'square '({
      north [east west]
      south [east west]
      east [north south]
      west [north south]}
      2)

    'cross '({
      north-east [north-east]
      north-west [north-west]
      south-east [south-east]
      south-west [south-west]}
      2)

    'plus '({
      north [north]
      south [south]
      east [east]
      west [west]}
      2)

    'diamond '({
      north-east [north-west south-east]
      north-west [north-east south-west]
      south-east [south-west north-east]
      south-west [south-east north-west]}
      2)

    'octagon '({
      north [north-east north-west]
      east [north-east south-east]
      south [south-east south-west]
      west [north-west south-west]
      north-east [north east]
      north-west [north west]
      south-east [south east]
      south-west [south west]}
    2)

    'duck '({
      north [north]
      south [south]
      east [east]
      west [west]
      north-east [north-east]
      north-west [north-west]
      south-east [south-east]
      south-west [south-west]}
      2)
})

(defn third [l]
  "Gets the third element of a list (like first and second)."
  (nth l 2))

(defn piece->movements [piece movement-for-pieces]
  "Looks at the piece and gives you its movement
  rules. This is needed because we figure out the
  movement rules by looking at various values
  like the type, colour, etc."
  
  (condp = (first piece) ;; piece type
    'stone (if (= (second piece) 'duck) ;; second is piece colour
              (movement-for-pieces 'duck)
              (movement-for-pieces 'stone))
    'trap (movement-for-pieces (third piece)) ;; third is piece subtype
    '(#{} 0))) ;; other things can't move

(defn possible-moves
  "Given a position, a board, and the way a piece moves, as well
  as how many (more) steps it can take, this function figures
  out all the other places the piece can possibly go in one
  move."

  ;; This is the simple version that we'll normally use; it's the same as
  ;; the other one but with some defaults set.
  ([start ;; our starting position
    piece ;; what (kind) of piece we're moving
    wrap ;; whether to wrap or not
    movement-for-pieces ;; rules for how the pieces move
    pieces-on-position ;; map of pieces and their position
    dir-to-coords] ;; maps human-readable directions to coordinates
    (let [[movements steps] (piece->movements piece movement-for-pieces)]
      (possible-moves 
        start
        piece
        steps ;; how many steps we can take
        #{} ;; nothing found already
        (keys movements) ;; first ever move
        wrap
        movements
        pieces-on-position
        roop-dir-to-coords)))
  
  ;; Here's the complicated recursed (cursed?) one
  ;; (Actually not cursed, because it worked on the first try! :D)
  ([start ;; the start square
    piece ;; the piece we're moving (for logic modification)
    steps-allowed ;; how many more steps it can take (decrements each iteration)
    found-already ;; moves that have been found already; no need to recalculate
    next-moves ;; the moves that the piece is allowed to take in the next step
    wrap ;; whether we should wrap or not
    movement-rules ;; movement rules for this piece (just the directions)
    pieces-on-position ;; list of all pieces and where they are
    dir-to-coords] ;; converts a direction to coordinates
    (if (<= steps-allowed 0)
      nil ;; abort and return an empty set
      (reduce into ;; functions will return a set of sets, so we flatten it
        (map ;; all the following runs on next-moves
          (fn [direction] ;; a direction in which we want to move
            (let [end (direction->valid-coordinates start direction wrap dir-to-coords)] ;; actual/absolute coordinates of our next move

                ;; now, we're processing that particular step
                (if (or ;; abort if ...
                   (found-already end) ;; ...we've found the direction already
                   (not (valid-position-or-nil end)) ;; ...it's an invalid position
                   (not (clear-pathway? start end pieces-on-position)) ;; ...pathway is blocked
                   (position-contains? end #{ 'stone 'trap 'bag } pieces-on-position)) ;; end position is occupied
                   nil ;; <-- that's the actual abort :)

                   ;; special logic for duck
                   (if (and (= (first piece) 'stone)
                            (= (second piece) 'duck)
                            (position-contains? end #{ 'dot } pieces-on-position))

                         ;; if it's a dot, we skip this move but still try for the next one
                         (possible-moves
                           end ;; stat for the next one
                           piece
                           (- steps-allowed 1) ;; one step less next time
                           (into found-already #{ end }) ;; previously found + ours
                           (movement-rules direction) ;; next moves for the direction we just went
                           wrap
                           movement-rules ;; all movement-rules
                           pieces-on-position ;; full piece-position details
                           dir-to-coords)

                         ;; if it's not a duck, we return this move joined with the next ones
                         (into #{ end }
                           (possible-moves
                             end ;; stat for the next one
                             piece
                             (- steps-allowed 1) ;; one step less next time
                             (into found-already #{ end }) ;; previously found + ours
                             (movement-rules direction) ;; next moves for direction we just went
                             wrap
                             movement-rules ;; all movement-rules
                             pieces-on-position ;; full piece-position details
                             dir-to-coords))))))
          next-moves))))) ;; next-moves is the thing we're mapping over, remember? ;)

(println "Okay, so the moves")

(println "The blue stone")
(println (possible-moves '(9 5) '(stone blue) false movement-for-pieces pieces-on-position roop-dir-to-coords))
(println (possible-moves '(5 7) '(stone duck) false movement-for-pieces pieces-on-position roop-dir-to-coords))
(println (possible-moves '(14 8) '(trap blue cross) false movement-for-pieces pieces-on-position roop-dir-to-coords))
(println (possible-moves '(6 16) '(trap green octagon) true movement-for-pieces pieces-on-position roop-dir-to-coords))

;; The experiment!
;; (println (possible-moves '(16 16) '(trap blue octagon) true movement-for-pieces pieces-on-position roop-dir-to-coords))

(defn end-formations
  "Finds formations that start at the given square, for
  the given kind of trap. This function is mainly meant
  to be looped over when checking all formations, but it
  might be useful by itself also. (It'll definitely be
  useful to have it separate while testing!)
  
  By the way, it assumes that a trap only moves two steps."
  [start movements pieces-on-position dir-to-coords]
  (reduce into
    (map (fn [middle-step]
           (let [start-stones (filter #(= (first %) 'stone) (pieces-on-position start))
                 start-stone (first start-stones)
                 middle (direction->valid-coordinates start middle-step dir-to-coords)
                 middle-stones (filter #(= (first %) 'stone) (pieces-on-position middle))
                 middle-stone (first middle-stones)
                 ] ;; assuming there's only one

                (cond ;; end should be a valid coordinate!
                      (nil? middle) nil

                      ;; start must contain only one of (any kind of) stone
                      (not (= (count start-stones) 1)) nil

                      ;; likewise for the middle
                      (not (= (count middle-stones) 1)) nil

                      ;; but the middle colour should be blue or green...
                      (not (#{'blue 'green} (second middle-stone))) nil

                      ;; ...and it should be different from the first!
                      (= (second start-stone) (second middle-stone)) nil

                      ;; those two taken care of, we take the next step!
                      :else (map (fn [last-step]
                                   (let [end (direction->valid-coordinates middle last-step dir-to-coords)
                                         end-stones (filter #(= (first %) 'stone) (pieces-on-position end))
                                         end-stone (first end-stones)]
                                         (cond ;; again, make sure it's a valid step
                                               (nil? end) nil

                                               ;; end must contain only one of (any kind of) stone
                                               (not (= (count end-stones) 1)) nil

                                               ;; end colour should be different from middle colour
                                               (= (second end-stone) (second middle-stone)) nil

                                               ;; finally, return the formation!
                                               :else end)))
                                 (movements middle-step)) ;; second iteration (last step) ends here
                                 
                                 ))) 
         (keys movements)))) ;; first iteration (middle step) ends here

;; Partial testing
(println "Partial")
(def pieces-just-one {
    '(15 11) ['(stone blue)]
    '(15 13) ['(stone green)]
    '(15 15) ['(stone blue)]
    '(11 15) ['(stone blue)]
    '(13 15) ['(stone green)]
    '(17 13) ['(stone blue)]
    '(17 11) ['(stone blue)]
  })
;; (println (end-formations '(15 15) (first (movement-for-pieces 'plus)) pieces-just-one roop-dir-to-coords))
;; (println (end-formations '(15 15) (first (movement-for-pieces 'square)) pieces-just-one roop-dir-to-coords))
;; (println (end-formations '(15 15) (first (movement-for-pieces 'octagon)) pieces-just-one roop-dir-to-coords))

(defn formations-from
  "Finds formations that can be created from the given
  position. (In normal gameplay, this would be called
  when a piece has just moved into the given position)."
  [start movement-for-pieces pieces-on-position dir-to-coords trap-types]
  (reduce into
    (map (fn [trap-type]
           ; for each of these trap types, we'll first check all
           ; the ways we can go forward and make a formation;
           ; then go backwards through anti-moves and see whether
           ; we can make formations with our start square in the
           ; middle.
           ;
           ; we'll send output for each of these options as a
           ; (possibly empty) vector, so we're using 'into'
           ; to join those two vectors together.
           (into
             [trap-type]
             [trap-type]
           ))
         trap-types)))

(println (formations-from '(12 13) movement-for-pieces pieces-on-position roop-dir-to-coords trap-types))


